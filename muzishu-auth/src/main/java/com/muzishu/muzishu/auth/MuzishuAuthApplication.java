package com.muzishu.muzishu.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.muzishu.muzishu.auth.domain.mapper")
public class MuzishuAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(MuzishuAuthApplication.class, args);
    }

}

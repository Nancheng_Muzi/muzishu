package com.muzishu.muzishu.auth;

import com.alibaba.druid.filter.config.ConfigTools;
import com.muzishu.framework.common.util.JsonUtils;
import com.muzishu.muzishu.auth.domain.dataobject.UserDO;
import com.muzishu.muzishu.auth.domain.mapper.UserDOMapper;
import jakarta.annotation.Resource;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;


@SpringBootTest
@Slf4j
class MuzishuAuthApplicationTests {


    @Resource
    private UserDOMapper userDOMapper;



    /**
     * 查询数据
     */
    @Test
    void testSelect() {
        // 查询主键 ID 为 4 的记录
        UserDO userDO = userDOMapper.selectByPrimaryKey(1L);
        log.info("User: {}", JsonUtils.toJsonString(userDO));
    }

    /**
     * Druid 密码加密
     */
    @SneakyThrows
    @Test
    void testEncodePassword() {
        // 你的密码
        String password = "Li-011202";
        String[] arr = ConfigTools.genKeyPair(512);

        // 私钥
        log.info("privateKey: {}", arr[0]);
        // 公钥
        log.info("publicKey: {}", arr[1]);

        // 通过私钥加密密码
        String encodePassword = ConfigTools.encrypt(arr[0], password);
        log.info("password: {}", encodePassword);
    }

}
